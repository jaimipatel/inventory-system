export default {
    methods: {

        // add data components methods

        //add data submit btn
        submit(data) {
            if (this.show == true) {
                this.submitAddItem(data);
                this.$router.push("/allitems");
            } else {
                this.submitAddCategory(data);
                this.$router.push("/allcategories");
            }
            alert("Data is Added")
        },

        //switch 
        changeSwitch() {
            this.switchStatus = !this.switchStatus;
        },


        //  show table methods

        //delete data from table
        deleteData(item) {
            if (confirm("Do you want to delete the data.")) {
                if (this.title == "All Item") {
                    this.deleteItemData(item);
                } else if (this.title == "All Categories") {
                    this.deleteCategoryData(item);
                } else {
                    alert("Error");
                }
            }
        },


        // update box methods

        //update data from table
        editData(data) {
            if (this.show == false) {
                this.editCategoryData();
            } else {
                this.editItemData();
            }

            if (data.status == false) {
                data.status = "Inactive";
            } else {
                data.status = "Active";
            }
            this.dialog = false;
            alert("Your data is updated.");
        },
    },

    computed: {

        // switch message computed property
        statusMessages() {
            return this.switchStatus ? ["Active"] : undefined;
        },
    },
}