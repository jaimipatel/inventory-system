import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        categoryShow: false,
        itemShow: true,
        itemsHeaders: [
            { text: "Id", align: "start", sortable: true, value: "id" },
            { text: "Name", value: "name" },
            { text: "Category", value: "category" },
            { text: "Description", value: "description", sortable: false },
            { text: "Price ( Rupee )", value: "price" },
            { text: "Status", value: "status" },
            { text: "Actions", value: "actions", sortable: false },
        ],
        categoriesHeaders: [
            { text: "Id", align: "start", sortable: true, value: "id" },
            { text: "Name", value: "name" },
            { text: "Description", value: "description", sortable: false },
            { text: "Status", value: "status" },
            { text: "Actions", value: "actions", sortable: false },
        ],
        itemsAllData: [{
                id: 2,
                name: "Clothes",
                category: 'Fashion',
                description: "Red t-shirt",
                price: 15,
                currency: 'Dollar',
                status: "Active",
            },
            {
                id: 1,
                name: "Mobile",
                category: 'Electronics',
                description: "Realme A9",
                price: 10000,
                currency: 'Rupee',
                status: "Inactive",
            },
        ],
        categoriesAllData: [{
                id: 2,
                name: "Clothes",
                description: "Red t-shirt",
                status: "Active",
            },
            {
                id: 1,
                name: "Mobile",
                description: "Realme A9",
                status: "Inactive",
            },
        ],
        itemAddData: {
            id: null,
            name: "",
            category: '',
            description: "",
            price: null,
            currency: '',
            status: "",
        },
        categoryAddData: {
            id: null,
            name: "",
            description: "",
            status: "",
        },
        selectCategoryItems: [
            "Electronics",
            "Fashion",
            "Cookware",
            "Good",
            "Furniture",
        ],
        selectCurrencyItems: ["Rupee", "Dollar"],
    },
    getters: {

    },
    mutations: {
        SUBMIT_ADD_ITEM(state, item) {
            state.itemAddData = item;
            if (state.itemAddData.status == false) {
                state.itemAddData.status = 'Inactive'
            } else {
                state.itemAddData.status = 'Active'
            }
            state.itemsAllData.push(state.itemAddData);
            state.itemAddData = {
                id: null,
                name: "",
                category: '',
                description: "",
                price: null,
                status: "",
            }
        },
        SUBMIT_ADD_CATEGORY(state, item) {
            state.categoryAddData = item;
            if (state.categoryAddData.status == false) {
                state.categoryAddData.status = 'Inactive'
            } else {
                state.categoryAddData.status = 'Active'
            }
            state.categoriesAllData.push(state.categoryAddData);
            state.categoryAddData = {
                id: null,
                name: "",
                description: "",
                status: "",
            }
        },
        DELETE_ITEM_DATA(state, item) {
            let index = state.itemsAllData.indexOf(item)
            state.itemsAllData.splice(index, 1)
        },
        DELETE_CATEGORY_DATA(state, item) {
            let index = state.categoriesAllData.indexOf(item)
            state.categoriesAllData.splice(index, 1)
        },
        EDIT_ITEM_DATA(state) {
            if (state.itemsAllData.status == false) {
                state.itemsAllData.status = 'Inactive'
            } else {
                state.itemsAllData.status = 'Active'
            }
        },
        EDIT_CATEGORY_DATA(state) {
            if (state.categoriesAllData.status == false) {
                state.categoriesAllData.status = 'Inactive'
            } else {
                state.categoriesAllData.status = 'Active'
            }
        }
    },
    actions: {
        submitAddItem({ commit }, item) {
            commit('SUBMIT_ADD_ITEM', item)
        },
        submitAddCategory({ commit }, item) {
            commit('SUBMIT_ADD_CATEGORY', item)
        },
        deleteItemData({ commit }, item) {
            commit('DELETE_ITEM_DATA', item)
        },
        deleteCategoryData({ commit }, item) {
            commit('DELETE_CATEGORY_DATA', item)
        },
        editItemData({ commit }) {
            commit('EDIT_ITEM_DATA')
        },
        editCategoryData({ commit }) {
            commit('EDIT_CATEGORY_DATA')
        }
    }
});