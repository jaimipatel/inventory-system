import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueRouter from 'vue-router'
import Routes from './router/routes'

import Store from './store/store'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: Routes,
    mode: 'history'
})


Vue.directive('textColor', (el, binding) => {
    el.style.fontWeight = 'bold';
    el.style.fontSize = '14px';
    if (binding.value == "Inactive") {
        el.style.color = 'orange'
    } else if (binding.value == "Active") {
        el.style.color = 'green'
    } else {
        el.style.color = 'aqua'
    }
})

Vue.filter('changePrice', (val) => {
    if (val.currency == 'Rupee') {
        return val.price
    } else if (val.currency == 'Dollar') {
        return Math.round(val.price * 75.24)
    }
})

Vue.config.productionTip = false

new Vue({
    store: Store,
    router: router,
    vuetify,
    render: h => h(App)
}).$mount('#app')