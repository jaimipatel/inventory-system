import Home from '../view/Home.vue';
import AddCategories from '../view/AddCategories.vue';
import AllCategories from '../view/AllCategories.vue';
import AddItem from '../view/AddItem.vue';
import AllItems from '../view/AllItems.vue';


export default [
    { path: '/', component: Home },
    { path: '/addcategory', component: AddCategories },
    { path: '/allcategories', component: AllCategories },
    { path: '/additem', component: AddItem },
    { path: '/allitems', component: AllItems },
    { path: '*', component: Home }
]